// auto generated side menu from top header menu start
var topHeaderMenu = $('header nav > ul').clone();
var sideMenu = $('.side-menu-wrap nav');
sideMenu.append(topHeaderMenu);
if ($(sideMenu).find('.sub-menu').length != 0) {
  $(sideMenu).find('.sub-menu').parent().append('<i class="fas fa-chevron-right d-flex align-items-center"></i>');
}
// auto generated side menu from top header menu end

// close menu when clicked on menu link start
// $('.side-menu-wrap nav > ul > li > a').on('click', function () {
//   sideMenuCloseAction();
// });
// close menu when clicked on menu link end

// open close sub menu of side menu start
var sideMenuList = $('.side-menu-wrap nav > ul > li > i');
$(sideMenuList).on('click', function () {
  if (!($(this).siblings('.sub-menu').hasClass('d-block'))) {
    $(this).siblings('.sub-menu').addClass('d-block');
  } else {
    $(this).siblings('.sub-menu').removeClass('d-block');
  }
});
// open close sub menu of side menu end

// side menu close start
$('.side-menu-close').on('click', function () {
  if (!($('.side-menu-close').hasClass('closed'))) {
    $('.side-menu-close').addClass('closed');
  } else {
    $('.side-menu-close').removeClass('closed');
  }
});
// side menu close end

// auto append overlay to body start
$('.wrapper').append('<div class="custom-overlay h-100 w-100"></div>');
// auto append overlay to body end

// open side menu when clicked on menu button start
$('.side-menu-close').on('click', function () {
  if (!($('.side-menu-wrap').hasClass('opened')) && !($('.custom-overlay').hasClass('show'))) {
    $('.side-menu-wrap').addClass('opened');
    $('.custom-overlay').addClass('show');
  } else {
    $('.side-menu-wrap').removeClass('opened');
    $('.custom-overlay').removeClass('show');
  }
})
// open side menu when clicked on menu button end

// close side menu when clicked on overlay start
$('.custom-overlay').on('click', function () {
  sideMenuCloseAction();
});
// close side menu when clicked on overlay end

// close side menu when swiped start
var isDragging = false, initialOffset = 0, finalOffset = 0;
$(".side-menu-wrap")
.mousedown(function(e) {
  isDragging = false;
initialOffset = e.offsetX;
})
.mousemove(function() {
  isDragging = true;
})
.mouseup(function(e) {
  var wasDragging = isDragging;
  isDragging = false;
finalOffset = e.offsetX;
  if (wasDragging) {
      if(initialOffset>finalOffset) {
         sideMenuCloseAction();
         }
  }
});
// close side menu when swiped end


function sideMenuCloseAction() {
  $('.side-menu-wrap').addClass('open');
  $('.wrapper').addClass('freeze');
  $('.custom-overlay').removeClass('show');
  $('.side-menu-wrap').removeClass('opened');
  $('.side-menu-close').removeClass('closed');
  $(sideMenuList).siblings('.sub-menu').removeClass('d-block');
}
// close side menu when clicked on overlay end

// close side menu over 992px start
  $(window).on('resize', function() {
      if($(window).width() >= 992) {
          sideMenuCloseAction();
      }
  })
  // close side menu over 992px end

  $('.owl-carousel1').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    animateOut: 'fadeOut',
    navText: [
      '<i class="fa fa-angle-left leftarrow" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right rightarrow" aria-hidden="true"></i>'
  ],
    responsive:{
        0:{
            dots:true,
            items:1
        },
        600:{
          dots:false,
            items:1
        },
        1000:{
          dots:false,
            items:1
        }
    }
    
});


$('.owl-carousel2').owlCarousel({
  loop: true,
  margin: 20,
  dots: false,
  responsiveClass: true,
  navText: [
    '<i class="fa fa-angle-left leftarrow2" aria-hidden="true"></i>',
    '<i class="fa fa-angle-right rightarrow2" aria-hidden="true"></i>'
],
  responsive: {
    0: {
      nav: false,
      items: 1,
      margin: 10,
      stagePadding: 20,
    },
    600: {
      nav: true,
      items: 3,
      margin: 20,
      stagePadding: 50,
    },
    1000: {
      nav: true,
      items: 4
    }
  }
});

$('.owl-carousel3').owlCarousel({
  loop: true,
  margin: 20,
  dots: false,
  autoplay:true,
  autoplaySpeed: 1000,
  autoplayHoverPause: false,
  responsiveClass: true,
  navText: [
    '<i class="fa fa-angle-left leftarrow3" aria-hidden="true"></i>',
    '<i class="fa fa-angle-right rightarrow3" aria-hidden="true"></i>'
],
  responsive: {
    0: {
      nav: false,
      items: 1,
      margin: 10,
      stagePadding: 20,
    },
    600: {
      nav: true,
      items: 3,
      margin: 20,
      stagePadding: 50,
    },
    1000: {
      nav: true,
      items: 4
    }
  }
});

$('.secondtab').on('click', function(){

  $(".secondcarousel").addClass('active');
  $(".firstcarousel").removeClass('active');
  $(".thirdcarousel").removeClass('active');
});

$('.thirdtab').on('click', function(){

$(".thirdcarousel").addClass('active');
$(".firstcarousel").removeClass('active');
$(".secondcarousel").removeClass('active');
});

$('.firsttab').on('click', function(){

$(".firstcarousel").addClass('active');
$(".secondcarousel").removeClass('active');
$(".thirdcarousel").removeClass('active');
});




window.onscroll = function() {myFunction()};
    
var navbar = document.getElementById("navbar1");
var sticky = navbar.offsetTop;


function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky");
    $("header .main-menu>li>a").addClass("addcolor");
    $("header .header-logo img").addClass("logochange");
  } else {
    navbar.classList.remove("sticky");
    $("header .main-menu>li>a").removeClass("addcolor");
    $("header .header-logo img").removeClass("logochange");
  }
}



const imgs = document.querySelectorAll('.img-select a');
const imgBtns = [...imgs];
let imgId = 1;

imgBtns.forEach((imgItem) => {
    imgItem.addEventListener('click', (event) => {
        event.preventDefault();
        imgId = imgItem.dataset.id;
        slideImage();
    });
});

function slideImage(){
    const displayWidth = document.querySelector('.img-showcase img:first-child').clientWidth;

    document.querySelector('.img-showcase').style.transform = `translateX(${- (imgId - 1) * displayWidth}px)`;
}

window.addEventListener('resize', slideImage);


$(".applybtn").hide();
$(".inputtext").keypress(function(){
  $(".applybtn").show();
});

//  ---------------------- start today code-------------------- 

function para1() {
  document.getElementById("displaydata").innerHTML = "Perfect for you and your best friend.";
  document.getElementById("quanty").innerHTML = "A$219.99";
  var element = document.getElementById("para1");
  element.classList.add("active");
  var element2 = document.getElementById("para2");
  element2.classList.remove("active");
  var element3 = document.getElementById("para3");
  element3.classList.remove("active");

}
function para2() {
  document.getElementById("displaydata").innerHTML = "Have a couple of pets? Look no further!";
  document.getElementById("quanty").innerHTML = "A$299.99";
  var element = document.getElementById("para2");
  element.classList.add("active");
  var element2 = document.getElementById("para1");
  element2.classList.remove("active");
  var element3 = document.getElementById("para3");
  element3.classList.remove("active");

}
function para3() {
  document.getElementById("displaydata").innerHTML = "More pets, the same level of protection.";
  document.getElementById("quanty").innerHTML = "A$369.99";
  var element = document.getElementById("para3");
  element.classList.add("active");
  var element2 = document.getElementById("para1");
  element2.classList.remove("active");
  var element3 = document.getElementById("para2");
  element3.classList.remove("active");
  

}
//  ---------------------- end today code-------------------- 



$("#Radar").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 0);
  $("#Radar").addClass("addstyle");
  $("#Real").removeClass("addstyle");
  $("#Virtual").removeClass("addstyle");
  $("#Sharing").removeClass("addstyle");
});
$("#Real").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 1);
  $("#Real").addClass("addstyle");
  $("#Radar").removeClass("addstyle");
  $("#Virtual").removeClass("addstyle");
  $("#Sharing").removeClass("addstyle");
});

$("#Virtual").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 2);
  $("#Virtual").addClass("addstyle");
  $("#Real").removeClass("addstyle");
  $("#Radar").removeClass("addstyle");
  $("#Sharing").removeClass("addstyle");
  
});
$("#Sharing").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 3);
  $("#Sharing").addClass("addstyle");
  $("#Real").removeClass("addstyle");
  $("#Radar").removeClass("addstyle");
  $("#Virtual").removeClass("addstyle");
  
});


$("#Radar1").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 0);
  $("#Radar1").addClass("addstyle");
  $("#Real1").removeClass("addstyle");
  $("#Virtual1").removeClass("addstyle");
  $("#Sharing1").removeClass("addstyle");
});
$("#Real1").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 1);
  $("#Real1").addClass("addstyle");
  $("#Radar1").removeClass("addstyle");
  $("#Virtual1").removeClass("addstyle");
  $("#Sharing1").removeClass("addstyle");
});

$("#Virtual1").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 2);
  $("#Virtual1").addClass("addstyle");
  $("#Real1").removeClass("addstyle");
  $("#Radar1").removeClass("addstyle");
  $("#Sharing1").removeClass("addstyle");
  
});
$("#Sharing1").on('click', function () {
  $('.owl-carousel').trigger('to.owl.carousel', 3);
  $("#Sharing1").addClass("addstyle");
  $("#Real1").removeClass("addstyle");
  $("#Radar1").removeClass("addstyle");
  $("#Virtual1").removeClass("addstyle");
  
});